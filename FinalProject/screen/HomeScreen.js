import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, TextInput, ScrollView, FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler';

import Fruit from '../screen/FruitScreen';


const HomeScreen = ({ navigation }) => {

    return (
        <View style={styles.container}>
            
            <View style={{backgroundColor: '#FFFFFF', height: 60, width: 360, marginBottom: 10}}>

                <View style={{flexDirection: 'row', marginTop: 10}}>

                    <View style={{flexDirection: 'row'}}>
                        
                        <TextInput style={styles.inputsearch} placeholder = "Search" />
                        <Image source={require('../assets/logo/icon/search.png')} style={styles.iconsearch} />
                        
                    </View>

                    <View style={{flexDirection: 'row', marginLeft: 195}}>
                        <Image source={require('../assets/logo/icon/Love.png')} style={styles.iconpendukung} />
                        <Image source={require('../assets/logo/icon/Pesan.png')} style={styles.iconpendukung} />
                        <Image source={require('../assets/logo/icon/Notif.png')} style={styles.iconpendukung} />
                    </View>
                    
                </View>

            </View>
            
            <ScrollView>
                <Image source={require('../assets/logo/home.png')} style={styles.fotohome} />

                <Text style={styles.headbuah}>Info Buah Segar</Text>

                <View style={{flexDirection: 'row'}}>

                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 1,
                        namabuah: 'Anggur',
                        keterangan: 'Anggur merupakan tanaman buah berupa perdu merambat yang termasuk ke dalam keluarga Vitaceae. Buah ini biasanya digunakan untuk membuat jus anggur, jelly, minuman anggur, minyak biji anggur dan kismis, atau dimakan langsung. Buah ini juga dikenal karena mengandung banyak senyawa polifenol dan resveratol yang berperan aktif dalam berbagai metabolisme tubuh, serta mampu mencegah terbentuknya sel kanker dan berbagai penyakit lainnya. Aktivitas ini juga terkait dengan adanya senyawa metabolit sekunder di dalam buah anggur yang berperan sebagai senyawa antioksidan yang mampu menangkal radikal bebas.',
                    })}>
                        <View style={styles.list1}>
                            <Image source={require('../assets/logo/icon/Anggur.png')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Anggur</Text>
                        </View>
                    </TouchableOpacity>

                    
                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 2,
                        namabuah: 'Jagung',
                        keterangan: 'Jagung (Zea mays ssp. mays) adalah salah satu tanaman pangan penghasil karbohidrat yang terpenting di dunia, selain gandum dan padi. Bagi penduduk Amerika Tengah dan Selatan, bulir jagung adalah pangan pokok, sebagaimana bagi sebagian penduduk Afrika dan beberapa daerah di Indonesia. Pada masa kini, jagung juga sudah menjadi komponen penting pakan ternak. Penggunaan lainnya adalah sebagai sumber minyak pangan dan bahan dasar tepung maizena. Berbagai produk turunan hasil jagung menjadi bahan baku berbagai produk industri farmasi, kosmetika, dan kimia.',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/Jagung.png')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Jagung</Text>
                        </View>
                    </TouchableOpacity>

                    
                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 3,
                        namabuah: 'Strawberry',
                        keterangan: 'Stroberi atau tepatnya stroberi kebun (juga dikenal dengan nama arbei, dari bahasa Belanda aardbei) adalah sebuah varietas stroberi yang paling banyak dikenal di dunia. Seperti spesies lain dalam genus Fragaria (stroberi), buah ini berada dalam keluarga Rosaceae. Secara umum buah ini bukanlah buah, melainkan buah palsu, artinya daging buahnya tidak berasal dari ovari tanaman (achenium) tetapi dari bagian bawah hypanthium yang berbentuk mangkuk tempat ovari tanaman itu berada.',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/Strawberry.png')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Strawberry</Text>
                        </View>
                    </TouchableOpacity>


                    <TouchableOpacity  onPress={ () => navigation.navigate('Detail', {
                        id: 4,
                        namabuah: 'Alpukat',
                        keterangan: 'Avokad atau Alpukat (Persea americana) ialah tumbuhan penghasil buah meja dengan nama sama. Tumbuhan ini berasal dari Meksiko dan Amerika Tengah dan kini banyak dibudidayakan di Amerika Selatan dan Amerika Tengah sebagai tanaman perkebunan monokultur dan sebagai tanaman pekarangan di daerah-daerah tropika lainnya di dunia.',
                    })}>
                        <View style={styles.list}>
                            <Image source={require('../assets/logo/icon/Peer.png')} style={styles.fotoicon} />
                            <Text style={styles.teksicon}>Alpukad</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <Text style={styles.headfresh}>Sayur Segar</Text>
            
                <Fruit/>

            </ScrollView>
        </View>
    )
}


export default HomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5F5F5',
        alignItems: 'center',
        marginTop: 35,
    },
    iconsearch: {
        width: 35,
        height: 35,
        marginTop: 2,
        marginLeft: -225,
    },
    inputsearch: {
        width: 240,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: '#A6A6A6',
        paddingLeft: 10,
        marginBottom: 10,
        marginLeft: 10,
        backgroundColor: '#F3F3F3',
        paddingLeft: 50
    },
    iconpendukung: {
        width: 35,
        height: 35,
        marginTop: 2,
    },
    fotohome: {
        width: 332,
        height: 190,
        borderRadius: 5,
        marginLeft: 13
    },
    list1: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14,
        marginLeft: 13,
    },
    list: {
        backgroundColor: '#FFFFFF',
        height: 110,
        width: 75,
        marginTop: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#EAEAEA',
        marginRight: 14
    },
    fotoicon: {
        marginTop: 5,
        marginLeft: 7,
        width: 60,
        height: 60,
        marginBottom: 10
    },
    teksicon: {
        color: '#000000',
        fontSize: 12,
        textAlign: 'center',
        fontWeight: 'bold'
    },
    headfresh: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    headbuah: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
    },
    datafruit: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12,
        flexDirection: 'row',
        marginBottom: 10
        
    },
    headvege: {
        marginLeft: 12,
        fontWeight: 'bold',
        color: '#139038',
        fontSize: 20,
        marginTop: 15,
        marginBottom: 5
    },
    datavege: {
        width: 120,
        height: 150,
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginRight: 10,
        marginLeft: 12
        
    },
})
