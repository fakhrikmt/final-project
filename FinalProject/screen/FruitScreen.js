import React, {Component} from 'react'
import { StyleSheet, Text, View, TouchableOpacity, Image, FlatList } from 'react-native';
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import Axios from 'axios';

export default class FruitScreen extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        };
    }

    // Mount User Method
    componentDidMount() {
        this.getGithubUser()
    }

    //   Get Api Users
    getGithubUser = async () => {
        try {
            const response = await Axios.get(`https://alfazza.xyz/final/final.php`)
            this.setState({ isError: false, isLoading: false, data: response.data })

        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render(){
        
        return (

            <FlatList
                data={this.state.data}
                renderItem={({ item }) =>
                    <View styles={{flex: 1}}>
                        <View style={styles.sayuran}>
                            <View style={styles.kotaksayur}>

                                <Image source={{ uri: `${item.fotosayur}` }} style={styles.fotosayur} />

                                <View style={{width: 170, height : 140}}>
                                    <View>
                                        <Text style={styles.namasayur}>{item.namasayur}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.keterangan}>{item.keteranganjual}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.harga}>{item.harga}</Text> 
                                    </View>
                                    
                                    <TouchableOpacity style={styles.kotakbeli}>
                                        <Text style={styles.beli}>Beli sayur</Text>
                                    </TouchableOpacity>
                                </View>

                                
                            </View>
                        </View>
                    </View>
                }
                keyExtractor={({ id }, index) => index}
            />

            
        )
    }
}



const styles = StyleSheet.create({
    sayuran: {
        backgroundColor: "#FFFFFF",
        padding: 10,
        borderRadius: 8,
        marginBottom: 10,
        marginTop:10,
        marginRight:5,
        width: 332,
        height: 160,
        marginLeft: 13,
        borderWidth: 1
    },
    kotaksayur: {
        height: 35,
        flexDirection: 'row',
        marginBottom:2,
        marginLeft: 13
    },
    namasayur: {
        color: '#003366',
        fontSize: 20,
        fontWeight: '700',
        marginTop: 10,
        marginLeft: 10,
    },
    harga: {
        color: '#901A13',
        fontSize: 18,
        fontWeight: "700",
        marginLeft: 10,
    },
    fotosayur: {
        width: 150,
        height: 150,
        marginLeft: -20,
        marginTop: -10
    },
    keterangan: {
        fontSize: 12,
        marginLeft: 10,
        marginTop: 10,
        marginBottom : 10,
    },
    kotakbeli: {
        backgroundColor: '#139038',
        marginLeft: 10,
        marginTop: 10,
        height: 28,
        borderRadius: 8,
    },
    beli: {
        marginLeft: 50,
        marginTop: 3,
        color: '#FFFFFF',
    }
})
