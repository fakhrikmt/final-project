import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

const AboutScreen = () => {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo/profil.png')} style={styles.fotoprofil} />

            <Text style={styles.teksnama}>La Jupriadi Fakhri</Text>
            <View style={styles.kotakkelas}><Text style={styles.tekskelas}>React Native Mobile</Text></View>

            <View style={styles.kotakprofil}>
                
                <View style={styles.medsoskotak}>
                    <FontAwesome5 name={'envelope'}
                        size={30} solid
                        color="#EFEFEF"
                        style={styles.posisiicon}
                    />
                    <Text style={styles.posisiteks}>fakhrikmt@gmail.com</Text>
                </View>

                <View style={styles.medsoskotak}>
                    <FontAwesome5 name={'facebook-square'}
                        size={30} solid
                        color="#EFEFEF"
                        style={styles.posisiicon}
                    />
                    <Text style={styles.posisiteks}>Fakhri Kmt</Text>
                </View>

                <View style={styles.medsoskotak}>
                    <FontAwesome5 name={'instagram'}
                        size={30} solid
                        color="#EFEFEF"
                        style={styles.posisiicon}
                    />
                    <Text style={styles.posisiteks}>@fakhri_kmt</Text>
                </View>

                <View style={styles.medsoskotak}>
                    <FontAwesome5 name={'twitter-square'}
                        size={30} solid
                        color="#EFEFEF"
                        style={styles.posisiicon}
                    />
                    <Text style={styles.posisiteks}>@fakhri_kmt</Text>
                </View>

                <View style={styles.medsoskotak}>
                    <FontAwesome5 name={'youtube-square'}
                        size={30} solid
                        color="#EFEFEF"
                        style={styles.posisiicon}
                    />
                    <Text style={styles.posisiteks}>Fakhri Kmt</Text>
                </View>

            </View>
        </View>
    )
}

export default AboutScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        marginTop: 40,
        backgroundColor: '#0E0663'
    },
    fotoprofil: {
        width: 200,
        height: 200,
        marginTop: 20
    },
    kotakprofil: {
        borderWidth: 1,
        borderRadius: 20,
        width: 360,
        height: 500,
        marginTop: 30,
        borderColor: '#F2F2F2',
        backgroundColor: '#FFFFFF'
    },
    teksnama: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#FFFFFF',
        marginTop: 20 
    },
    kotakkelas: {
        backgroundColor: '#139038',
        width: 170,
        height: 30,
        borderRadius: 20,
        marginTop: 5
    },
    tekskelas: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold',
        marginTop: 5
    },
    medsoskotak: {
        marginTop: 20, 
        marginRight: 30, 
        backgroundColor: '#139038', 
        borderRadius: 10, 
        width: 280, 
        height: 40, 
        flexDirection: 'row',
        marginLeft: 38
    },
    posisiicon: {
        marginLeft: 25,
        marginTop: 4
    },
    posisiteks: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 16,
        marginLeft: 10,
        marginTop: 8
    }
})
