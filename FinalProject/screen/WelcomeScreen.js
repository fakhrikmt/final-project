import React from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'

const WelcomeScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Image source={require('../assets/logo/welcome.png')} style={styles.logowelcome} />
            
            <View style={styles.tekswel}>
                <Text style={styles.tekswelcome}>Welcome</Text>
            </View>

            <View style={styles.teksjud}>
                <Text style={styles.teksjud1}>Aplikasi penjualan sayur segar, higienis, murah dan amanah</Text>
                <Text style={styles.teksjud2}>khusus daerah Kota Sorong, Papua Barat</Text>
            </View>

            <View style={styles.btn}>
                <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('Login') }>
                    <Text style={styles.btnlogteks}>Sign In</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.btnsign} onPress={ () => navigation.navigate('Register') }>
                    <Text style={styles.btnsignteks}>Sign Up</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.menuvia}>
                <Text style={styles.via}>atau login via sosial media</Text>

                <View style={styles.logovi}>
                    <Image source={require('../assets/logo/fb.png')} style={styles.logofb} />
                    <Image source={require('../assets/logo/go.png')} style={styles.logogo} />
                </View>
            </View>
        </View>
    )
}

export default WelcomeScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: 50
    },
    logowelcome: {
        width: 350,
        height: 350
    },
    tekswel: {
        marginTop: 30,
    },
    tekswelcome: {
        fontWeight: 'bold',
        fontSize: 30,
        color: '#139038'
    },
    teksjud: {
        marginTop: 20,
    },
    teksjud1: {
        fontSize: 12,
        color: '#656565',
        textAlign: 'center'
    },
    teksjud2: {
        textAlign: 'center',
        fontSize: 12,
        color: '#656565',
        marginTop: 5
    },
    btn: {
        marginTop: 40,
        flexDirection: 'row'
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#24903B',
        borderRadius: 20,
        width: 120,
        height: 40,
        marginRight: 30
    },
    btnlogteks: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    btnsign: {
        padding: 10,
        backgroundColor: '#FFFFFF',
        borderRadius: 20,
        width: 120,
        height: 40,
        borderWidth: 2
    },
    btnsignteks: {
        textAlign: 'center',
        color: '#000000',
        fontWeight: 'bold',
        marginTop: -2
    },
    menuvia: {
        marginTop: 40
    },
    via: {
        textAlign: "center",
        fontSize: 12,
        color: '#656565'
    },
    logovi: {
        flexDirection: 'row',
        marginTop: 10
    },
    logofb: {
        width: 40,
        height: 38,
        alignItems: 'center',
        marginLeft: 30
    },
    logogo: {
        width: 40,
        height: 40,
        alignItems: 'center'
    }
})
