import React from 'react'
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native'

const LoginScreen = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.tekslogin}>Welcome Back!</Text>          

            <Image source={require('../assets/logo/login.png')} style={styles.logologin} />

            <View style={styles.formusername}>
                <Text style={styles.teksform}>Username</Text>
                <TextInput style={styles.input}/>
            </View>

            <View>
                <Text style={styles.teksform}>Password</Text>
                <TextInput style={styles.input} secureTextEntry={true}/>
                <Text style={styles.teksformforgot}>Forgot Password</Text>
            </View>

            <View style={{marginTop: 30, marginLeft: 30}}>
                <TouchableOpacity style={styles.btnlog} onPress={ () => navigation.navigate('DrawerStackScreen')} >
                    <Text style={styles.btnlogteks}>Login</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.menuvia}>
                <Text style={styles.via}>atau login via sosial media</Text>

                <View style={styles.logovi}>
                    <Image source={require('../assets/logo/fb.png')} style={styles.logofb} />
                    <Image source={require('../assets/logo/go.png')} style={styles.logogo} />
                </View>

                <View style={{flexDirection: 'row', marginTop: 10}}>
                    <Text style={styles.via}>Dont’t have an account?</Text><Text style={styles.via1} onPress={ () => navigation.navigate('Register')} > Sign Up</Text>
                </View>
            </View>
        </View>
    )
}

export default LoginScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: 40,
    },
    tekslogin: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#139038',
    }, 
    logologin: {
        width: 250,
        height: 250
    },
    teksform: {
        color: '#139038',
        fontWeight: 'bold',
        marginBottom: 5
    },
    teksformforgot: {
        color: '#139038',
        fontWeight: 'bold',
        marginTop: 5,
        textAlign: 'right'
    },
    input: {
        width: 300,
        height: 40,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#A6A6A6',
        paddingLeft: 10
    },
    formusername: {
        marginTop: 20,
        marginBottom: 20
    },
    btnlog: {
        padding: 10,
        backgroundColor: '#24903B',
        borderRadius: 20,
        width: 130,
        height: 40,
        marginRight: 30
    },
    btnlogteks: {
        textAlign: 'center',
        color: '#FFFFFF',
        fontWeight: 'bold'
    },
    menuvia: {
        marginTop: 30
    },
    via: {
        textAlign: "center",
        fontSize: 14,
        color: '#656565'
    },
    via1: {
        textAlign: "center",
        fontSize: 14,
        color: '#139038',

    },
    logovi: {
        flexDirection: 'row',
        marginTop: 5,
        marginLeft: 30
    },
    logofb: {
        width: 40,
        height: 38,
        alignItems: 'center',
        marginLeft: 30
    },
    logogo: {
        width: 40,
        height: 40,
        alignItems: 'center'
    }
})
